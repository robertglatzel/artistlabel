module.exports = {
  purge: [
    './src/**/*.html',
    './src/**/*.vue',
    './src/**/*.jsx',
  ],
  darkMode: false, // or 'media' or 'class'
  theme: {
    extend: {
      colors: {
        bandcampWhite: {
          '100': '#FFFFFF',
          '200': '#EEF2F3'
        },
        bandcampBlue: {
          '200': '#4390A8',
          '300': '#428194',
          '400': '#477987'
        }
      }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [],
}
