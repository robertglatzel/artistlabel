import config from '/src/config.js'

const _cookie = {
  set ( name, value, days = 365 ) {
    name = config.STORAGE_PREFIX + name

    let expires
    if ( days ) {
      let date = new Date()
      date.setTime( date.getTime() + ( days * 24 * 60 * 60 * 1000 ) )
      expires = '; expires=' + date.toGMTString()
    } else {
      expires = ''
    }
    document.cookie = name + '=' + value + expires + '; domain=' + window.location.hostname + '; path=/'
  },

  get ( name ) {
    name = config.STORAGE_PREFIX + name
    const nameEQ = name + '='
    const ca = document.cookie.split( ';' )
    for ( let i = 0; i < ca.length; i++ ) {
      let c = ca[ i ]
      while ( c.charAt( 0 ) === ' ' ) {
        c = c.substring( 1, c.length )
      }
      if ( c.indexOf( nameEQ ) === 0 ) {
        return c.substring( nameEQ.length, c.length )
      }
    }
    return null
  },

  del ( name ) {
    name = config.STORAGE_PREFIX + name
    this.set( name, '', -1 )
  }
}

const _localStorage = {
  set: ( name, value ) => window.localStorage.setItem( config.STORAGE_PREFIX + name, value ),
  get: name => window.localStorage.getItem( config.STORAGE_PREFIX + name ),
  del: name => window.localStorage.removeItem( config.STORAGE_PREFIX + name )
}

const _sessionStorage = {
  set: ( name, value ) => window.sessionStorage.setItem( config.STORAGE_PREFIX + name, value ),
  get: name => window.sessionStorage.getItem( config.STORAGE_PREFIX + name ),
  del: name => window.sessionStorage.removeItem( config.STORAGE_PREFIX + name )
}

const storage = config.STORAGE_TYPE === 'cookie' ? _cookie : ( window.localStorage ? _localStorage : _cookie )

export const cookie = _cookie
export const localStorage = _localStorage
export const sessionStorage = _sessionStorage
export default storage
