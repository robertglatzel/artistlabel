const STORAGE_PREFIX = 'BC-csv-reader'
const STORAGE_TYPE = 'cookie' // localStorage - Should always be cookie for iOS users

export default {
  STORAGE_TYPE,
  STORAGE_PREFIX: STORAGE_PREFIX + '_',
}
