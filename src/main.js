import { createApp } from 'vue'
import App from './App.vue'
import router from './router'
import './index.css'

createApp( App ).use( router ).mount( '#app' )
// app.config.errorHandler = ( err, vm, info ) => {
//   router.push( '/404' )
// }
// app.mount( '#app' )

