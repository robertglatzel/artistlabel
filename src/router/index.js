import { createWebHistory, createRouter } from 'vue-router'
import SummaryPage from '/src/components/SummaryPage/SummaryPage.vue'
import FileReader from '/src/components/FileReader/FileReader.vue'
import LabelConfig from '/src/components/LabelConfig/LabelConfig.vue'
import ErrorPage from '/src/components/ErrorPage/ErrorPage.vue'

const routes = [
	{
		path: '/',
		name: 'Home',
		component: FileReader
	},
	{
		path: '/configuration',
		name: 'Configuration',
		component: LabelConfig
	},
	{
		path: '/summary',
		name: 'Summary',
		component: SummaryPage
	},
	{
		path: '/404',
		name: 'Error',
		component: () => ErrorPage
	}
]

const router = createRouter( {
	history: createWebHistory(),
	routes
} )

export default router
