import _ from 'lodash'
import { ref } from 'vue'
import { localStorage } from '/src/utils/storage.js'

const productionCosts = ref( {} )
const configHasBeenSet = ref( false )

const globalPercents = ref( {
	label: { physical: null, digital: null },
	artist: { physical: null, digital: null }
} )

const getProductionCostsInStorage = () => {
	const data = localStorage.get( 'productionCost-storage' )
	if ( data ) {
		productionCosts.value = { ...productionCosts.value, ...JSON.parse( data ) }
	}
}

const getGlobalPercentsInStorage = () => {
	const data = localStorage.get( 'globalPercent-storage' )
	if ( data ) {
		globalPercents.value = { ...globalPercents.value, ...JSON.parse( data ) }
	}
}

const getSpecificPercentsInStorage = () => {
	const data = localStorage.get( 'specificPercent-storage' )
	return data
}

const setGlobalPercents = () => {
	localStorage.set(
		`globalPercent-storage`,
		JSON.stringify( globalPercents.value )
	)
}

const setProductionCosts = () => {
	localStorage.set(
		`productionCost-storage`,
		JSON.stringify( productionCosts.value )
	)
}

const setConfig = () => {
	setGlobalPercents()
	setProductionCosts()
	configHasBeenSet.value = true
}

const setArtistSpecificConfig = ( name, value ) => {
	const previousData = getSpecificPercentsInStorage()
	localStorage.set(
		`specificPercent-storage`,
		JSON.stringify( { ...JSON.parse( previousData ), [ name ]: value } )
	)
}

const splitFormatting = text => {
	// Used to format the production cost items
	const splitText = text.split( '||' )
	return { itemName: _.first( splitText ), physicalName: _.last( splitText ) }
}

const useLabelConfig = () => {
	return {
		getProductionCostsInStorage,
		getGlobalPercentsInStorage,
		getSpecificPercentsInStorage,
		setConfig,
		setArtistSpecificConfig,
		globalPercents,
		productionCosts,
		configHasBeenSet,
		splitFormatting,
		setProductionCosts
	}
}

export default useLabelConfig
