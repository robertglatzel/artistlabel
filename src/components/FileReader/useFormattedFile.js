import _ from 'lodash'
import { computed } from 'vue'
import useFileReader from './useFileReader.js'

const { savedCsvText } = useFileReader()

const csvToArray = computed( () => {
	return JSON.parse( savedCsvText.value )
} )

const artistList = computed( () => {
	// List of artists found in this csv file.
	return _.filter(
		_.compact( _.keys( groupByArtistAndPayout.value ), item => item !== 'payout' )
	).join( ', ' )
} )


const groupByArtistAndPayout = computed( () => {
	const filteredArray = _.reject( csvToArray.value, item => {
		return _.get( item, [ 'artist' ] ) === '' && _.get( item, [ 'item type' ] ) === 'payout'
	} )

	const ordered = _.orderBy( filteredArray, item => { return _.get( item, [ 'artist' ] ) } )

	return _.groupBy( ordered, item => {
		return _.get( item, [ 'artist' ] )
	} )
} )

const groupByRelease = computed( () => {
	return _.mapValues( groupByArtistAndPayout.value, value => {
		return _.groupBy( value, item => {
			return _.get( item, [ 'item name' ] )
		} )
	} )
} )

const productionCostGrouping = computed( () => {
	return _.compact(
		_.map( groupByArtistAndPayout.value, ( values, artist ) => {
			const physicalItems = _.filter(
				_.compact( _.uniq( _.map( values, item => {
					if ( item[ 'item type' ] === 'package' ) {
						return `${ item[ 'item name' ] } || ${ item[ 'package' ] }`
					}
				} ) ) ),
				entry => entry.length !== 0 )
			if ( physicalItems.length === 0 ) {
				return
			}
			// Create an object from the array.
			const productionCostObject = _.reduce( physicalItems, ( obj, key ) => ( { ...obj, [ key ]: null } ), {} )

			return { [ artist ]: productionCostObject }
		} )
	)
} )

const detectedCurrency = computed( () => {
	return _.get( _.first( csvToArray.value ), [ 'currency' ] )
} )


const currentCsvDates = computed( () => {
	let startingDate = _.first( _.first( csvToArray.value )[ 'date' ].split( ' ' ) )
	let endingDate = _.first( _.last( csvToArray.value )[ 'date' ].split( ' ' ) )
	return { startingDate, endingDate }
} )

const useFormattedFile = () => {
	return {
		artistList,
		csvToArray,
		groupByRelease,
		currentCsvDates,
		groupByArtistAndPayout,
		productionCostGrouping,
		detectedCurrency
	}
}

export default useFormattedFile
