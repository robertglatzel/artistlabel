import _ from 'lodash'
import { ref, computed, watch } from 'vue'
import convertCSVToArray from 'convert-csv-to-array'
import { localStorage } from '/src/utils/storage.js'

const rawCsvText = ref( null )
const savedCsvText = ref( null )
const fileUploaded = ref( false )
const csvFileName = ref( null )

watch( csvFileName, () => {
	// Pull the label name out of the csv file.
	if ( csvFileName.value ) {
		csvFileName.value = _.first( csvFileName.value.split( '_' ) ).split( '-' ).join( ' ' )
	}
} )

const setCsvInStorage = file => {
	// Set the raw csv in local storage
	localStorage.set( `csv-storage`, file )
}

const setCsvNameStorage = fileName => {
	// Set the raw csv in local storage
	localStorage.set( `csv-fileName`, fileName )
}

const resetCsvFile = () => {
	// Remove the file in local storage, reset fields to accept new csv.
	try {
		localStorage.del( 'csv-storage' )
		localStorage.del( 'csv-fileName' )
		fileUploaded.value = false
		savedCsvText.value = null
		rawCsvText.value = null
	} catch ( e ) {
		console.log( e )
	}
}

watch(
	rawCsvText,
	() => {
		const readFromStorage = localStorage.get( 'csv-storage' )
		const readNameFromStorage = localStorage.get( 'csv-fileName' )
		if ( _.isNull( rawCsvText.value ) ) {
			if ( _.isNull( readFromStorage ) ) {
				//  No file has been uploaded and no file is present in the local storage.
				return
			} else if ( !_.isNull( readFromStorage ) ) {
				// No file has been uploaded but there is a stored file in local storage.
				csvFileName.value = readNameFromStorage
				savedCsvText.value = readFromStorage
				fileUploaded.value = true
			}
		} else if ( !_.isNull( rawCsvText.value ) ) {
			// A file has been uploaded, so check it to be valid, then if so, save it to storage.
			const filteredCsvToArrayOutcome = filteredCsvToArray.value
			if ( savedCsvText.value ) {
				// If there is a saved CSV file, filter the new array if any duplicate entries are found in the saved csv.
				const duplicateFreeArray = _.reject( filteredCsvToArrayOutcome, line => {
					return _.find( JSON.parse( savedCsvText.value ), [ 'bandcamp transaction id', line[ 'bandcamp transaction id' ] ] )
				} )
				setCsvInStorage( JSON.stringify( duplicateFreeArray ) )
				savedCsvText.value = JSON.stringify( duplicateFreeArray )
			} else {
				setCsvInStorage( JSON.stringify( filteredCsvToArrayOutcome ) )
				savedCsvText.value = JSON.stringify( filteredCsvToArrayOutcome )
			}
			setCsvNameStorage( csvFileName.value )
			fileUploaded.value = true
		}
	},
	{ deep: true, immediate: true }
)

const filteredCsvToArray = computed( () => {
	// Filter out sensitive columns before saving csv in local storage.
	if ( !_.isNull( rawCsvText.value ) ) {
		const csvToArr = convertCSVToArray( rawCsvText.value, {
			header: false,
			type: 'object',
			separator: ','
		} )

		const filteredOutSensitiveData = _.map( csvToArr, entry => {
			return _.omit( entry, [ 'paid to', 'buyer name', 'buyer email', 'buyer phone', 'buyer note', 'ship to name', 'ship to street', 'ship to street 2', 'ship to city', 'ship to state', 'ship to zip', 'ship to country', 'ship to country code', 'buyer country code', 'buyer country name', 'ship date' ] )
		} )
		return filteredOutSensitiveData
	}
} )

const csvValidityCheck = ( rawUploadedFile ) => {
	// Filter out sensitive columns before saving csv in local storage.
	if ( !_.isNull( rawUploadedFile ) ) {
		try {
			// Around 32 columns are in the bandcamp csv with all values present.
			const top32Sample = _.take( rawUploadedFile.split( ',' ), 32 )
			// 16 values in total
			const validColumns = [ 'balance of revenue share', 'bandcamp transaction id', 'artist', 'package', 'collected revenue share', 'catalog number', 'net amount', 'item name', 'item type', 'item url', 'paypal transaction id', 'payout balance', 'item price', 'date', 'currency', 'transaction fee' ]
			const columnValidityCheck = _.map( top32Sample, entry => validColumns.includes( entry ) ? entry : null )
			return _.uniq( _.compact( columnValidityCheck ) ).length === 16
		} catch ( e ) {
			console.log( e )
			return false
		}
	}
}

const useFileReader = () => {
	return {
		rawCsvText,
		savedCsvText,
		fileUploaded,
		setCsvInStorage,
		resetCsvFile,
		csvFileName,
		csvValidityCheck
	}
}

export default useFileReader
